#!/bin/bash
set -euxo pipefail

core_version=1.10.1

sudo useradd -M -s /bin/false coredns
cd ~
wget https://github.com/coredns/coredns/releases/download/v$core_version/coredns_"$core_version"_linux_amd64.tgz
tar -xvf coredns_"$core_version"_linux_amd64.tgz
sudo mv coredns /usr/local/bin
sudo chown coredns:coredns /usr/local/bin/coredns
sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/coredns

## COpy to /etc/systemd/system/coredns.service
sudo cp /vagrant/hack/coredns.service /etc/systemd/system/coredns.service
sudo systemctl daemon-reload

## Create dir for Corefile
sudo mkdir -p /etc/coredns

## Install resolvconf
sudo apt install resolvconf -y

## Copy Corefile
sudo cp /vagrant/hack/Corefile /etc/coredns/Corefile

## Follow steps 4 in: https://dev.to/n1try/how-to-enable-dns-over-tls-on-ubuntu-using-coredns-18mp